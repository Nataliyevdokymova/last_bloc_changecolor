class StateN {
  StateN();

  factory StateN.changeColor(bool isRed) = ColorProjectState;
}

class ColorProjectState extends StateN {
  final bool isRed;

  ColorProjectState(this.isRed);
}
